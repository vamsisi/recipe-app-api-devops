# Recipe App API DevOps Starting Point

The course teaches how to build a fully functioning REST API using:

 - Python
 - Django / Django-REST-Framework
 - Docker / Docker-Compose
 - Test Driven Development

## Getting started

To start project, run:

```
docker-compose up
```

The API will then be available at http://127.0.0.1:8000 

## Geeting started production version on local machine

To start projecct, run 

``` 
docker-compose build app 
```
``` 
docker-compose -f docker-compose-proxy.yml build 
````
``` 
docker-compose -f docker-compose-proxy.yml up
```

Since, this API receive request through Nginx proxy, you need to have nginx proxy image in our local machine

````
docker pull 1813354/nginx-proxy
````

run -- docker images to see if this image is pulled succesfully for dockerhub

Handy commands you may need 
````
docker login 
docker build -t <imagename> .
````
