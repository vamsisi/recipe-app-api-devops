#!/bin/sh

#print output logs during failure
set -e

python manage.py collectstatic --noinput

#whenever you make any changes to the database models they reflect back into db when running application
#making sure db is running 
#pull the db every sec and when it is available access and continuous to next command
python manage.py wait_for_db


#run migration on our database
python manage.py migrate

#explantion availble in .txt file 
uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi
